package salesforce

//oauth config
const (
	grantType    = "password"
	loginURI     = "https://login.salesforce.com/services/oauth2/token"
	testLoginURI = "https://test.salesforce.com/services/oauth2/token"

	//invalid session status code
	invalidSessionErrorCode = "INVALID_SESSION_ID"
)

//auth request/response data
type sfOauth struct {
	AccessToken string `json:"access_token"`
	InstanceURL string `json:"instance_url"`
	ID          string `json:"id"`
	IssuedAt    string `json:"issued_at"`
	Signature   string `json:"signature"`

	clientID      string
	clientSecret  string
	refreshToken  string
	userName      string
	password      string
	securityToken string
	environment   string
}
