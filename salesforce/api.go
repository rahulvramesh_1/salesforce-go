package salesforce

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"bitbucket.org/evhivetech/salesforce-go/sobjects"
)

//SFApi - sales force api object
type SFApi struct {
	ApiVersion string
	Oauth      *sfOauth
}

//CreateLead - create  new lead
func (sfApi *SFApi) CreateLead(lead sobjects.Lead) (sobjects.LeadResponse, error) {

	response, err := sfApi.postLeadToSF(lead)

	if err != nil {
		return sobjects.LeadResponse{}, fmt.Errorf("Error Creating lead: %v", err)

		//check if its auth error , then reauth

	}

	return response, nil
}

//PostLeadToSF - common post
func (sfApi *SFApi) postLeadToSF(lead sobjects.Lead) (sobjects.LeadResponse, error) {

	requestURL := sfApi.Oauth.InstanceURL + leadURL

	//check for the date colum
	//better way is to change this methid to marshal / umarhsal

	//covert it into map

	log.Printf("%+v\n", lead)
	payload, _ := json.Marshal(lead)

	// var inInterface map[string]interface{}

	// finalPaylod := make(map[string]interface{})
	// json.Unmarshal(payload, &inInterface)

	// for field, val := range inInterface {
	// 	//check for data is emplty or not
	// 	t := reflect.TypeOf(val)

	// 	switch t.Kind() {
	// 	case reflect.String:
	// 		if val != "" && val != "0001-01-01T00:00:00Z" {
	// 			//add to interface
	// 			finalPaylod[field] = val
	// 		}
	// 	}
	// }

	// payload1, _ := json.Marshal(finalPaylod)
	payloadString := bytes.NewReader(payload)
	req, err := http.NewRequest("POST", requestURL, payloadString)
	if err != nil {
		return sobjects.LeadResponse{}, fmt.Errorf("Error doing the post request: %v", err)
	}

	req.Header.Set("User-Agent", userAgent)
	req.Header.Set("Content-Type", contentType)
	req.Header.Set("Accept", responseType)
	req.Header.Set("Authorization", fmt.Sprintf("%v %v", "Bearer", sfApi.Oauth.AccessToken))

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return sobjects.LeadResponse{}, fmt.Errorf("Error Occured : %v", err)

	}

	defer res.Body.Close()
	log.Printf("%+v\n", res.Body)
	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return sobjects.LeadResponse{}, fmt.Errorf("a: %v", err)
	}

	apiError := &ApiErrors{}
	if err := json.Unmarshal(body, apiError); err == nil {
		if apiError.Validate() {
			log.Println(apiError.String())
			return sobjects.LeadResponse{}, fmt.Errorf("%v", apiError.String())

		}
	}

	//wrap the response
	reponse := sobjects.LeadResponse{}

	if err := json.Unmarshal(body, &reponse); err != nil {
		return sobjects.LeadResponse{}, fmt.Errorf("c %v", err)
	}

	//reponse the object

	return reponse, nil
}

//AssignCampaignToLead - add lead to campaign
func (sfApi *SFApi) AssignCampaignToLead(leadCampaign sobjects.CampaignLead) error {

	requestURL := sfApi.Oauth.InstanceURL + addLeadToCampaginURL

	payload, _ := json.Marshal(leadCampaign)

	payloadString := bytes.NewReader(payload)

	req, err := http.NewRequest("POST", requestURL, payloadString)
	if err != nil {
		return fmt.Errorf("Error doing the post request: %v", err)
	}

	req.Header.Set("User-Agent", userAgent)
	req.Header.Set("Content-Type", contentType)
	req.Header.Set("Accept", responseType)
	req.Header.Set("Authorization", fmt.Sprintf("%v %v", "Bearer", sfApi.Oauth.AccessToken))

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		return fmt.Errorf("Error doing the post request: %v", err)
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return fmt.Errorf("Error doing the post request: %v", err)
	}

	apiError := &ApiErrors{}
	if err := json.Unmarshal(body, apiError); err == nil {
		if apiError.Validate() {
			return fmt.Errorf("Error doing the post request: %v", apiError.String())
		}
	}

	reponse := sobjects.LeadResponse{}

	if err := json.Unmarshal(body, &reponse); err != nil {
		return fmt.Errorf("Error doing the post request: %v", err)
	}

	return nil
}
