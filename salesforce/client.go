package salesforce

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

//generic constants
const (
	version      = "1.0.0"
	userAgent    = "salesforce-go" + version
	contentType  = "application/json"
	responseType = "application/json"

	loginURL             = "https://login.salesforce.com/services/oauth2/token"
	leadURL              = "/services/data/v44.0/sobjects/Lead"
	addLeadToCampaginURL = "/services/data/v44.0/sobjects/CampaignMember"
)

// NewClient creates a new client
func NewClient(
	version,
	clientID,
	clientSecret,
	userName,
	password,
	securityToken string) (*SFApi, error) {

	var sfapi SFApi

	//set basic auth information
	oauth := &sfOauth{
		clientID:      clientID,
		clientSecret:  clientSecret,
		userName:      userName,
		password:      password,
		securityToken: securityToken,
	}

	//authenticate
	err := authenticate(oauth)

	if err != nil {
		return nil, err
	}

	sfapi.ApiVersion = version
	sfapi.Oauth = oauth

	return &sfapi, nil

}

//authenticate - oauth request to salesforce
func authenticate(oauth *sfOauth) error {

	payload := url.Values{
		"grant_type":    {grantType},
		"client_id":     {oauth.clientID},
		"client_secret": {oauth.clientSecret},
		"username":      {oauth.userName},
		"password":      {fmt.Sprintf("%v%v", oauth.password, oauth.securityToken)},
	}

	// Build Body
	body := strings.NewReader(payload.Encode())

	// Build Request
	req, err := http.NewRequest("POST", loginURL, body)
	if err != nil {
		return fmt.Errorf("Error creating authentication request: %v", err)
	}

	req.Header.Set("User-Agent", userAgent)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Accept", responseType)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("Error sending authentication request: %v", err)
	}
	defer resp.Body.Close()

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("Error reading authentication response bytes: %v", err)
	}

	// Attempt to parse response as a force.com api error
	apiError := &ApiError{}
	if err := json.Unmarshal(respBytes, apiError); err == nil {
		// Check if api error is valid
		if apiError.Validate() {
			return apiError
		}
	}

	if err := json.Unmarshal(respBytes, oauth); err != nil {
		return fmt.Errorf("Unable to unmarshal authentication response: %v", err)
	}

	log.Println(oauth.AccessToken)

	return nil

}
